delete from DETALLESOLICITUD;
delete from SOLICITUD;
delete from ARTICULO;
delete from CATEGORIA;
delete from EMPLEADO;
delete from PROVEEDOR;
delete from rechazados;

insert into PROVEEDOR
values('P1','IREX','12345678','irex@correo.com');

insert into PROVEEDOR
values('P2','SABEMAS','12345678','sabemas@correo.com');

insert into PROVEEDOR
values('P3','ELREY','12345678','elrey@correo.com');

insert into PROVEEDOR
values('P4','LIMPIATEX','12345678','limpiaTex@correo.com');        

insert into CATEGORIA
values('C1','Categoria1',1000,null);

insert into CATEGORIA
values('C2','Categoria2',1000,null);

insert into CATEGORIA
values('C3','Categoria3',1000,null);

insert into CATEGORIA
values('C4','Categoria4',9000,null);

insert into CATEGORIA
values('C5','Categoria5',6000,null);

insert into CATEGORIA
values('C6','Categoria6',4000,null);

insert into CATEGORIA
values('C7','Categoria7',2300,null);

insert into CATEGORIA
values('C8','Categoria8',1500,null);

insert into CATEGORIA
values('C9','Categoria9',1000,null);

insert into CATEGORIA
values('C10','Categoria10',2000,null);

insert into ARTICULO
values('A1','P1','C1','Articulo1','Escoba','Patito',100,null);

insert into ARTICULO
values('A2','P1','C2','Articulo2','Pala','Patito',200,null);

insert into ARTICULO
values('A3','P1','C3','Articulo3','Caretillo','Patito',300,null);

insert into ARTICULO
values('A4','P2','C1','Articulo4','Caretillo','Patito',500,null);

insert into ARTICULO
values('A5','P2','C2','Articulo5','Caretillo','Patito',600,null);

insert into ARTICULO
values('A6','P2','C3','Articulo4','Caretillo','Patito',700,null);

INSERT INTO EMPLEADO
VALUES('E1','Jared','Mars','j@gmail.com');

INSERT INTO EMPLEADO
VALUES('E2','Jerome','Yackey','y@gmail.com');

INSERT INTO EMPLEADO
VALUES('E3','Bryan','Stober','b@gmail.com');

INSERT INTO EMPLEADO
VALUES('E4','Ron','Stober','r@gmail.com');

INSERT INTO EMPLEADO
VALUES('E5','Fabricio','Villegas','f@gmail.com');

INSERT INTO EMPLEADO
VALUES('E6','Gareth','Hopper','g@gmail.com');

INSERT INTO EMPLEADO
VALUES('E7','Koji','Narama','k@gmail.com');

INSERT INTO EMPLEADO
VALUES('E8','Brad','Wylo','b@gmail.com');

INSERT INTO EMPLEADO
VALUES('E9','Ronnie','Radkie','rd@gmail.com');

INSERT INTO EMPLEADO
VALUES('E10','Wylo','Radkie','dj@gmail.com');

commit;


