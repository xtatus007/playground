CREATE OR REPLACE PROCEDURE TEST AS 

   empleadoExistente VARCHAR(20);
   inventario INT;
   articulo CHAR(3);
   solicitud number;
   total INT;
/*Cursors*/
   c_peticionId peticiones.peticionId%type;
   c_empleadoId peticiones.empleadoId%type; 
   c_categoriaId peticiones.categoriaId%type; 
   c_cantidad peticiones.cantidad%type;
   c_estado peticiones.estado%type;
   c_ejecucion peticiones.ejecucion%type;
   
    cursor c_peticiones IS 
    select * from peticiones; 
BEGIN
    OPEN c_peticiones; 
        LOOP
            FETCH c_peticiones INTO c_peticionId, c_empleadoId, c_categoriaId, c_cantidad, c_estado,c_ejecucion;
            EXIT WHEN c_peticiones%NOTFOUND;
            DBMS_OUTPUT.PUT_LINE('PETICION->' || c_peticionId || 'EMPLEADO:' || c_empleadoId || ' CATEGORIA:' || c_categoriaId || ' CANTIDAD:' || c_cantidad);
            select IdEmpleado INTO  empleadoExistente  FROM EMPLEADO WHERE IdEmpleado = c_empleadoId;
            IF(empleadoExistente IS NOT NULL) THEN
            DBMS_OUTPUT.PUT_LINE('Empleado->' || empleadoExistente ||' encontrado.');
            SELECT cantidad, idArticulo INTO inventario, articulo FROM ARTICULO WHERE CATEGORIA =c_categoriaId;
            total := inventario - c_cantidad;
            DBMS_OUTPUT.PUT_LINE('total->' || total);
                  IF (total >= 0) THEN 
                    DBMS_OUTPUT.PUT_LINE('DISPONIBLE->Cantidad Solicitada: ' || c_cantidad || 'Cantidad En Inventario: ' || inventario);
                    UPDATE ARTICULO
                    SET CANTIDAD = CANTIDAD - c_cantidad
                    where CATEGORIA=c_categoriaId;
                    
                    UPDATE PETICIONES 
                    SET ESTADO=1, EJECUCION='Exitosa'
                    where PETICIONID=c_peticionId;
                    
                    INSERT INTO Solicitud (empleado)values(c_empleadoId)
                    RETURNING ID into solicitud;
                    
                    INSERT INTO detalleSolicitud (articulo,solicitud,cantidad) values(articulo,solicitud,c_cantidad);
                ELSE 
                    DBMS_OUTPUT.PUT_LINE('NO DISPONIBLE->Cantidad Solicitada: ' || c_cantidad || 'Cantidad En Inventario: ' || inventario);
                    UPDATE PETICIONES 
                    SET ESTADO=1, EJECUCION='Fallo'
                    where PETICIONID=c_peticionId;  
                END IF;
            ELSE
            DBMS_OUTPUT.PUT_LINE('EMPLEADO NO REGISTRADO EN LA BASE DE DATOS.');
            END IF;
        END LOOP;
      
    CLOSE c_peticiones;
      EXCEPTION WHEN NO_DATA_FOUND THEN
            DBMS_OUTPUT.PUT_LINE('EMPLEADO NO DISPONIBLE EN BASE DE DATOS');
END TEST;
