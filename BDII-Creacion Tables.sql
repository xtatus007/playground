drop sequence sol_seq;
drop TRIGGER sol_bir;
drop table detalleSolicitud;
drop table solicitud;
drop table articulo;
drop table empleado;
drop table categoria;
drop table proveedor;


create table proveedor(
    idProveedor CHAR(2),
    nombre VARCHAR(45),
    cedulaJuridica VARCHAR(45),
    contact VARCHAR(45),
    CONSTRAINT PK_PROVEEDOR 
        PRIMARY KEY (idProveedor)
);

create table categoria(
    idCategoria CHAR(3), 
    descripcion VARCHAR(45),
    precio NUMBER,
    expira DATE,
    CONSTRAINT PK_CATEGORIA 
        PRIMARY KEY (idCategoria)
);

create table empleado(
    idEmpleado CHAR(3),
    nombre VARCHAR(45),
    apellido VARCHAR(45),
    correo VARCHAR(45),
    CONSTRAINT PK_EMPLEADO 
        PRIMARY KEY(idEmpleado)
);

create table articulo(
    idArticulo CHAR(3),
    proveedor CHAR(2),
    categoria CHAR(3),
    descripcion VARCHAR(45),
    marcar VARCHAR(45),
    modelo VARCHAR(45),
    cantidad INT,
    fechaCompra DATE,
    CONSTRAINT PK_ARTICULO
        PRIMARY KEY(idArticulo),
    CONSTRAINT FK_PROVEEDOR
        FOREIGN KEY(proveedor) 
        REFERENCES proveedor(idProveedor),
    CONSTRAINT FK_CATEGORIA 
        FOREIGN KEY(categoria) 
        REFERENCES categoria(idCategoria)
);

create table solicitud(
    ID NUMBER(10),
    empleado CHAR(3),
    fechaInicio DATE,
    fechatFinal DATE,
    CONSTRAINT PK_SOLICITUD
        PRIMARY KEY(ID),
    CONSTRAINT FK_EMPLEADO 
        FOREIGN KEY(empleado)
        REFERENCES empleado(idEmpleado)
);

create table detalleSolicitud(
    articulo CHAR(3),
    solicitud number(10),
    CONSTRAINT FK_ARTICULO FOREIGN KEY(articulo) REFERENCES articulo(idArticulo),
    CONSTRAINT FK_SOLICITUD FOREIGN KEY(solicitud) REFERENCES solicitud(ID),
    cantidad INT,
    PRIMARY KEY(articulo,solicitud)
);
CREATE SEQUENCE sol_seq;

CREATE OR REPLACE TRIGGER sol_bir 
BEFORE INSERT ON solicitud 
FOR EACH ROW
WHEN (new.id IS NULL)
BEGIN
  :new.id := sol_seq.NEXTVAL;
END;



