drop table peticiones;

create table peticiones (
    peticionId CHAR(5), 
    empleadoId CHAR(2),
    categoriaId CHAR(2),
    cantidad INT,
    estado INT,
    ejecucion VARCHAR(10)
);

insert into peticiones
values ('PT1','E1','C2',650,0,NULL);

insert into peticiones
values ('PT2','E2','C2',200,0,NULL);

insert into peticiones
values ('PT3','E3','C1',700,0,NULL);

insert into peticiones
values ('PT4','E1','C3',50,0,NULL);

insert into peticiones
values ('PT5','E3','C3',100,0,NULL);

insert into peticiones
values ('PT6','E2','C3',200,0,NULL);

insert into peticiones
values ('PT7','E4','C2',300,0,NULL);

insert into peticiones
values ('PT8','E4','C3',500,0,NULL);

insert into peticiones
values ('PT9','E8','C3',50,0,NULL);

insert into peticiones
values ('PT10','E6','C4',10,0,NULL);
